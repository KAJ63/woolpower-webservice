var mysql = require('mysql');

var con = mysql.createConnection({
	host: "localhost",
	user: "woolpower",
	password: "woolpower",
	database: "woolpower"
});

exports.findFirst = function(sql, callback) {

    con.query(sql, (err,result) => {
    	  if(err) {
    		  throw err;
    	  }
    	  return callback(result[0]);
    	});
}
